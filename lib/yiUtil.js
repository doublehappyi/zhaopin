/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 13-10-16
 * Time: 上午11:08
 * To change this template use File | Settings | File Templates.
 */


var Yi = {};
Yi.BLANK_SPACE = " ";
Yi.EMPTY_STRING = "";

Yi.debug = function (str) {
    if (window.console && window.console.log) {
        console.log(str);
    }
}
Yi.emptyFn = function () {
    //empty function: do nothing
}

Yi.getCurrWinWidth = function () {
    return $(window).width();
}

Yi.getCurrWinHeight = function () {
    return $(window).height();
}

Yi.getDomCssValue = function ($dom, cssProperty) {
    return parseInt($dom.css(cssProperty));
}

//使用这个函数的时候，如果margin-left等值必须显式的设置好，否则在ie8下会出现NaN的问题！
Yi.getDomRealWidth = function ($dom) {
    var domRealWidth = $dom.width() +
        Yi.getDomCssValue($dom, "margin-left")+
        Yi.getDomCssValue($dom, "margin-right")+
        Yi.getDomCssValue($dom, "padding-left") +
        Yi.getDomCssValue($dom, "padding-right");

    return domRealWidth;
}
Yi.getDomRealHeight = function ($dom) {
    var domRealHeight = $dom.height() +
        Yi.getDomCssValue($dom, "margin-top") +
        Yi.getDomCssValue($dom, "margin-bottom") +
        Yi.getDomCssValue($dom, "padding-top") +
        Yi.getDomCssValue($dom, "padding-bottom");

    return domRealHeight;
}

Yi.getDomMaxWidth = function($dom){
    var maxWidth = 0;
    for(var i = 0; i < $dom.length; i++){

        if($($dom[i]).width() > maxWidth){
            maxWidth = $($dom[i]).width();
        }
    }

    return maxWidth;
}

Yi.isAnimating = function ($dom) {
    return $dom.is(":animated");
}
Yi.getActiveIndex = function($dom, dom) { //dom是当前被点击的dom，那navItem是一个包含了dom的jquery对象。
    var activeIndex = 0;
    for (var i = 0; i < $dom.length; i++) {
        if ($dom[i] === dom) {
            activeIndex = i;
        }
    }
    return activeIndex;
}

Yi.shortHand = function(str,length){
    var str = str || Yi.EMPTY_STRING,
        length = length || 6;
    if(str.length > length){
        str = str.substring(0,length) + "...";
    }
    Yi.debug(str.length);
    return str;
}

Yi.trimStringRigthLeftSpace = function(str){
    return str.replace(/(^\s*|\s*$)/g,"");
}

Yi.trimStringAllSpace = function(str){
    return str.replace(/\s/g,"");
}/**
 * Created by db on 14-9-13.
 */
