/**
 * Created by YI on 2014/9/13.
 */
$(function(){
    var $applyBtn = $("#apply-btn");
    var $layerBg = $("#layer-bg"),
        $layer = $("#layer"),
        $time = $(".time"),
        layerBgHeight = $layerBg.height();

    var intervalId ;

    console.log(layerBgHeight ,$layer.height());


    $applyBtn.click(function(){
        //弹窗代码，请放在ajax请求的成功回调函数里面
        $layerBg.fadeIn(500);
        $layer.css({
            marginTop:(layerBgHeight - $layer.height())/2
        });

        var i = 3;
        $time.text(i);
        intervalId = setInterval(function(){
            i = i-1;
            $time.text(i);
            if(i == 0){
                location = "index.html";
            }
        },1000);
    });

    $(".layer-close").click(function(){
        clearInterval(intervalId);
        $layerBg.fadeOut();
    });

    $layer.click(function(e){
        e.stopPropagation();
        return false;
    });
});