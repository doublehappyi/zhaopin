/**
 * Created by db on 14-8-31.
 */
$(function () {
    var outer = $(".swiper-container-outer").swiper({
        mode: "vertical"
    });

    var inner_1 = $("#inner_1").swiper({
            mode: "horizontal"
        }),
        inner_2 = $("#inner_2").swiper({
            mode: "horizontal"
        });

    var inners = [inner_1, inner_2];

    for (var i = 0; i < inners.length; i++) {
        bindEvents(inners[i]);
    }

    outer.addCallback("SlideChangeEnd", function (swiper) {
        console.log('swiper SlideChangeEnd end...');
    });


    function bindEvents(inner) {

        //导航的点击动作
        var innerContainer = inner.container;
        var $nav = $(innerContainer).next(".bottom").find(".nav");
        $nav.click(function () {
            var me = this,
                index = getActiveIndex($nav, me);
            inner.swipeTo(index + 1);
            $nav.removeClass("nav-active");
            $(me).addClass("nav-active");
        });

        //swiper的滑动结束回调函数
        inner.addCallback("SlideChangeEnd", slideChangeEnd);


    }

    function slideChangeEnd(swiper, direction) {
        var $nav = $(swiper.container).next(".bottom").find(".nav");
        var activeIndex = swiper.activeIndex;
        $nav.removeClass("nav-active");
        if (activeIndex !== 0) {
            $($nav[activeIndex - 1]).addClass("nav-active");
        }
    }

    function getActiveIndex($dom, dom) {
        var index = 0;
        for (var i = 0; i < $dom.length; i++) {
            if ($dom[i] == dom) {
                index = i;
                break;
            }
        }
        return index;
    }

    function setSwiperStatus(swiper, activeIndex) {
        if (activeIndex !== 0) {
            $($nav[activeIndex - 1]).addClass("nav-active");
        }
    }
});