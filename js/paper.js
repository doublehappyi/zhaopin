(function () {

    var winH = $(window).height(),
        winW = $(window).width();

    //console.log("winH : ", winH, "winW: ", winW);

    var unitW = parseInt(winW / 7),//单元宽度为7个，其中2个预留空
        unitH = parseInt(winH / 10);//单位高度为10个
    var pointCoords = [
        [0, 0],
        [unitW-10, unitH+5],
        [unitW * 2 , unitH * 2+15],
        [unitW * 3 +10 , unitH * 3 - 15],
        [unitW * 4 + 20, unitH * 4 - 20],
        [unitW * 5+30, unitH * 5],
        [unitW * 5 + 20, unitH * 6],
        [unitW * 4 + 10, unitH * 7],
        [unitW * 3 , unitH * 8],
        [unitW * 2 - 10, unitH * 9],
        [unitW, winH]
    ];
    var path = new Path();
    path.strokeColor = '#9c8376';
    path.strokeWidth = 3;

    var points = [],circles = [],point,circle,
        circleR = 7;
    for(var i = 0; i < pointCoords.length;i++){
        point = new Point(pointCoords[i][0], pointCoords[i][1]);

        if(i !== 0 && i !== (pointCoords.length - 1)){
            circle = new Path.Circle(point,7);
            circle.fillColor = "#d45e16";
        }else{
           /* circle = new Path.Circle(point,0);
            circle.fillColor = "#000000";*/
        }

        path.add(point);

        points.push(point);
        circles.push(circle);
    }
    path.smooth();

    $(function(){
        var $city = $(".city");

        $("#layer-bg").css({
            left:0,
            top:0
        });
        resizeCityBg();
        //各城市的摆放位置
        positionCities();
        bindEvents();

        function bindEvents(){
            var $city = $(".city");
            var $layerBg = $("#layer-bg"),
                $layer = $("#layer"),
                layerBgHeight = $layerBg.height();

            console.log(layerBgHeight ,$layer.height());
            $city.click(function(){
                $layerBg.fadeIn(500);
                $layer.css({
                    marginTop:(layerBgHeight - $layer.height())/2
                });
            });

            $(".layer-close").click(function(){
                $layerBg.fadeOut();
            });

            $layer.click(function(e){
                e.stopPropagation();
                return false;
            });
        }

        function resizeCityBg(){
            //获取city元素的上下和左右的pading值
            var paddingTop = parseInt($city.css("padding-top")),
                paddingLeft = parseInt($city.css("padding-left"));

            //重置一下city的背景图片尺寸：是元素高宽度和padding值之和
            $city.css("background-size",($city.width() + paddingLeft*2) + "px " + ($city.height() + paddingTop*2)+"px");

            //由于背景图片的角落，导致文字看起来不居中，这里矫正一下：3个像素
            $city.filter(".city-left").css({
                paddingLeft:paddingLeft+3,
                paddingBottom:paddingTop+3
            });

            $city.filter(".city-right").css({
                paddingRight:paddingLeft+3,
                paddingBottom:paddingTop+3
            });
        }

        function positionCities() {
            var $currCity ,left,top;

            for (var i = 0; i < $city.length; i++){
                $currCity = $($city[i]);
                if($currCity.hasClass("city-left")){
                    left = pointCoords[i+1][0] + circleR;
                    top = pointCoords[i+1][1] - $currCity.height() - parseInt($currCity.css("padding"))*2 - circleR
                }else {
                    left = pointCoords[i+1][0] - $currCity.width() - parseInt($currCity.css("padding"))*2 - circleR;
                    top = pointCoords[i+1][1] - $currCity.height() -parseInt($currCity.css("padding"))*2 - circleR
                }
                $currCity.css({
                    left:left,
                    top:top
                });
            }
        }
    });

   /* path.add(new Point(0, 0));

    path.add(new Point(100, 100));
    var c = new Path.Circle(new Point(100, 100), 10);
    c.fillColor = "red";*/


    /*var t = new Path.Rectangle(new Point(100 + 20, 200), 50, 30)
    t.fillColor = "red"
    path.add(new Point(100, 200));*/

// Smooth the segments of the copy:


})();