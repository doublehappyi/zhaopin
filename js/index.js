/**
 * Created by db on 14-8-31.
 */


window.onload = function(){

    /*定义一些全局使用的变量*/
    var outer = $("#outer").swiper({
        mode: "vertical"
    });

    var inner_1 = $("#inner_1").swiper({
            mode: "horizontal"
        }),
        inner_2 = $("#inner_2").swiper({
            mode: "horizontal"
        }),
        inner_3 = $("#inner_3").swiper({
            mode: "horizontal"
        }),
        inner_4 = $("#inner_4").swiper({
            mode: "horizontal"
        });

    var inners = [inner_1, inner_2, inner_3, inner_4];

    //初始化一些样式，以及绑定事件
    init();
    bindEvents();

    //程序初始化完毕
    $("#loading").addClass("fadeOut").css("display","none");
    $("#outer").addClass("fadeIn");

    //outer.swipeTo(8);

    function init() {
        //设置inner swiper的高度
        var innerSwiperHeight = outer.height - Yi.getDomRealHeight($(".bottom"));
        console.log("outer.height ",outer.height,".bottom : ",Yi.getDomRealHeight($(".bottom")),"innerSwiperHeight : ",innerSwiperHeight);
        $(".swiper-container-inner").height(innerSwiperHeight);
        //设置文字展示区的高度
        var jobContentHeight = innerSwiperHeight -
            Yi.getDomRealHeight($(".job-title"))
            - Yi.getDomRealHeight($(".container")) - 20;

        console.log("jobContentHeight : ",jobContentHeight);
        $(".job-content").height(jobContentHeight);

        //首页
        //校园招聘首页
        //187,235通过比例计算出人物应该距离底部有多高。
        var bottomHeight = $("#school-box").height() * 187 / 235;

        $("#p_1").css({
            bottom: bottomHeight
        });

        $("#p_2").css({
            bottom: bottomHeight + 5
        });

        //初始化center-cube位置
        /*var $centerCube = $("#center-cube"),
         $container = $centerCube.closest(".slide-content");
         var containerH = $(window).height(),
         containerW = $(window).width(),
         cubeH = $centerCube.height(),
         cubeW = $centerCube.width();
         $centerCube.css({
         top:(containerH - cubeH)/2,
         left:(containerW - cubeW)/2
         });*/


        //计算社会招聘放置顶部位置
        addIndexAnimate();

        var $city = $(".city"),
            len = $city.length,
            i = 0;
        setInterval(function(){
            if(i < len){
                if(i > 0){
                    $($city[i - 1]).removeClass("breathe");
                }
                $($city[i]).addClass("breathe");
            }else{
                $($city[i - 1]).removeClass("breathe");
                i = 0;
                $($city[i]).addClass("breathe");
            }

            i++;
        },4000);


        //加入我们的图片高宽度修改
        var $img = $("#join_us"),
            $imgCtn = $("#join_us_ctn");
        var imgW = $img.width(),
            imgH = $img.height(),
            ctnW = $imgCtn.width(),
            ctnH = $imgCtn.height(),
            winH = $(window).height(),
            winW = $(window).width();

        //alert($(window).width()+"aaa"+$(window).height()+"screen");

        if(imgW/imgH < ctnW/ctnH){
            $img.height(winH*0.9).css({
                marginTop:winH*0.05,
                marginLeft:(winW - $("#join_us").width())/2
            });
        }else{
            $img.width(winW*0.9).css({
                marginLeft:winW*0.05,
                marginTop:(winH - $("#join_us").height())/2
            });
        }
    }

    function bindEvents() {
        //窗口缩放事件
        $(window).resize(function () {
            $(".swiper-container-inner").animate({height: outer.height - $(".bottom").height()}, 200);
        });

        //循环绑定内部swiper事件监听
        for (var i = 0; i < inners.length; i++) {
            innerbindEvents(inners[i]);
        }
        //外部swiper事件监听
        outer.addCallback("SlideChangeEnd", outerSlideChangeEnd);
        /*外部swipe滑动导航*/
        $("#society-zp").click(function () {
            outer.swipeNext();
        });
        $("#school-zp").click(function () {
            outer.swipeTo(6);
        });
        $("#engineer").click(function () {
            outer.swipeNext();
        });
        $("#function").click(function () {
            outer.swipeTo(3)
        });
        $("#market").click(function () {
            outer.swipeTo(4)
        });
        $("#finance").click(function () {
            outer.swipeTo(5)
        });
    }

    document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
        //监听一下这个按钮
        WeixinJSBridge.on('menu:share:timeline', function(){
            WeixinJSBridge.invoke('shareTimeline', {
                //这下面是配置，可以自己随便配置,但是一定要配置完全！
                "img_url": "http://qqfood.tc.qq.com/meishio/16/4585bf7c-be04-420f-ac8a-2dba61a7561f/0",//换成你们自己的logo地址
                "img_width": "400",
                "img_height": "400",
                "link": "http://10.1.28.11:9090/zhaopin/index.html",
                "desc": "这里是一段描述...",
                "title": "这里是一个title，会显示在分享到朋友圈"
            }, function (res) {
                //这里是回调函数
                //alert("over")
            });
        });

    }, false);
    
    //outer.swipeTo(9);
    /*function outerIndex() {

     }
     */
    function innerbindEvents(inner) {
        //导航的点击动作
        var innerContainer = inner.container;
        var $bottomNav = $(innerContainer).next(".bottom").find(".bottom-nav");
        $bottomNav.click(function () {
            var me = this,
                index = getActiveIndex($bottomNav, me);
            inner.swipeTo(index + 1);
            $bottomNav.find(".nav").removeClass("nav-active");
            $(me).find(".nav").addClass("nav-active");

            $bottomNav.find(".nav-triangle").removeClass("nav-triangle-active");
            $(me).find(".nav-triangle").addClass("nav-triangle-active");


        });

        //swiper的滑动结束回调函数
        inner.addCallback("SlideChangeEnd", innerSlideChangeEnd);

        //每个页面的导航
        var $index = $(innerContainer).find(".inner-index");
        if ($index.length > 0) {
            $index.click(function () {
                var clickedIndex = getActiveIndex($index, this);
                inner.swipeTo(clickedIndex + 1);
            });
        }
    }

    function innerSlideChangeEnd(swiper, direction) {
        var $container = $(swiper.container);
        var $nav = $container.next(".bottom").find(".nav"),
            $navTriangle = $container.next(".bottom").find(".nav-triangle");
        var activeIndex = swiper.activeIndex;
        $nav.removeClass("nav-active");
        $navTriangle.removeClass("nav-triangle-active")
        if (activeIndex !== 0) {
            $($nav[activeIndex - 1]).addClass("nav-active");
            $($navTriangle[activeIndex - 1]).addClass("nav-triangle-active");
            $container.prev(".kind-bg-ctn").animate({"opacity": 0.2},300);
        } else {
            $container.prev(".kind-bg-ctn").animate({"opacity": 1},300);
        }
    }

    function outerSlideChangeEnd(swiper, direction) {
        var activeIndex = swiper.activeIndex;
        removeClassesAnimation();
        removeSchoolAnimate();
        resetOtherClassPosition(activeIndex);
        removeIndexAnimate();
        removeCurrClassAnimation();
        /*添加动画类*/
        if (activeIndex == 6) {
            addSchoolAnimate();
        } else if (activeIndex == 2 || activeIndex == 3 || activeIndex == 4 || activeIndex == 5) {
            addCurrClassAnimation(activeIndex);
        } else if (activeIndex == 1) {
            addClassesAnimation();
        } else if (activeIndex == 0) {
            addIndexAnimate();
        } else {

        }
    }

    function getActiveIndex($dom, dom) {
        var index = 0;
        for (var i = 0; i < $dom.length; i++) {
            if ($dom[i] == dom) {
                index = i;
                break;
            }
        }
        return index;
    }

    function setSwiperStatus(swiper, activeIndex) {
        if (activeIndex !== 0) {
            $($nav[activeIndex - 1]).addClass("nav-active");
        }
    }

    function addSchoolAnimate() {
        //187,235通过比例计算出人物应该距离底部有多高。

        $("#p_1").addClass("bounceInDown").css("opacity", 1);
        $("#p_2").addClass("bounceInUp delay-200").css("opacity", 1);

        $("#p_2").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $("#zhi_1").addClass("lightSpeedIn");
            $("#nan").addClass("lightSpeedIn delay-200");
            $("#zhi_2").addClass("lightSpeedIn delay-400");
            $("#nv").addClass("lightSpeedIn delay-600");
        });

        $("#nv").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            var $text = $(".text");
            for (var i = 0; i < $text.length; i++) {
                $($text[i]).addClass("fadeInRight delay-" + (i * 100));
            }
        });
    }

    function addIndexAnimate() {
        var $cube = $("#cube"),
            $society = $("#society-zp"),
            $school = $("#school-zp"),
            $society_p = $("#society-p"),
            $school_p = $("#school-p");

        $cube.addClass("animate").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            var position_top = $("#position-top").offset();
            var position_bottom = $("#position-bottom").offset();

            var top = {
                top: position_top.top - $society.height(),
                left: position_top.left - $society.width()
            }

            $society.css(top);
            $school.css(position_bottom);
            //社会招聘
            $society_p.css({
                top: position_top.top - $society_p.height() / 2,
                left: position_top.left - $society_p.width() / 2
            }).addClass("fadeInDown duration-500").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                var $line = $society.find("img");
                var lineHeight = $line.height();
                $line.animate({
                    opacity: 1
                }, {
                    easing: "easeInCubic",
                    duration: 1000,
                    complete: function () {
                        $society.find("h1").addClass("lightSpeedIn delay-500");
                    }
                });
            });
            //校园招聘
            $school_p.css({
                top: position_bottom.top - $school_p.height() / 2,
                left: position_bottom.left - $school_p.width() / 2
            }).addClass("fadeInUp duration-500").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                var $line = $school.find("img");
                $line.animate({
                    opacity: 1
                }, {
                    easing: "easeInCubic",
                    duration: 1000,
                    complete: function () {
                        $school.find("h1").addClass("lightSpeedIn delay-500");
                    }
                });
            });
        });
    }

    function addClassesAnimation() {
        var $societyNav = $(".society-nav");

        $("#center-cube").addClass("rotateIn").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            for (var i = 0; i < $societyNav.length; i++) {
                if (i % 2 == 0) {
                    $($societyNav[i]).addClass("fadeInLeft");
                } else {
                    $($societyNav[i]).addClass("fadeInRight");
                }

            }
        });


    }

    function addCurrClassAnimation(activeIndex) {
        //kinde-title的序号比activeIndex的序号小2
        var $kindTitle = $(".kind-title"),
            $kindBg = $(".kind-bg");
        $($kindTitle[activeIndex - 2]).addClass("swing");
        $($kindBg[activeIndex - 2]).addClass("fadeIn");
    }

    function removeCurrClassAnimation(){
        var $kindTitle = $(".kind-title"),
            $kindBg = $(".kind-bg");
        $kindTitle.removeClass("swing");
        $kindBg.removeClass("fadeIn");
    }

    function removeIndexAnimate() {
        var $cube = $("#cube"),
            $society = $("#society-zp"),
            $school = $("#school-zp"),
            $society_p = $("#society-p"),
            $school_p = $("#school-p"),
            $school_line = $school.find("img"),
            $society_line = $society.find("img");

        $cube.removeClass("animate");
        $society_p.removeClass("fadeInDown");
        $school_p.removeClass("fadeInUp");
        $school_line.css("opacity", 0);
        $society_line.css("opacity", 0);

        $society.find("h1").removeClass("lightSpeedIn");
        $school.find("h1").removeClass("lightSpeedIn");
    }

    function removeSchoolAnimate() {
        //移除文字：欢迎。。。
        var $text = $(".text");
        for (var i = 0; i < $text.length; i++) {
            $($text[i]).removeClass("fadeInRight delay-" + (i * 100));
        }

        $("#p_1").removeClass("bounceInDown").css("opacity", 0);
        $("#p_2").removeClass("bounceInUp delay-200").css("opacity", 0);
        $("#zhi_1").removeClass("lightSpeedIn");
        $("#nan").removeClass("lightSpeedIn delay-200");
        $("#zhi_2").removeClass("lightSpeedIn delay-400");
        $("#nv").removeClass("lightSpeedIn delay-600");
    }

    function removeClassesAnimation() {
        /*移除各动画类和属性*/
        $("#center-cube").removeClass("rotateIn");

        var $societyNav = $(".society-nav");
        for (var i = 0; i < $societyNav.length; i++) {
            if (i % 2 == 0) {
                $($societyNav[i]).removeClass("fadeInLeft");
            } else {
                $($societyNav[i]).removeClass("fadeInRight");
            }
        }
    }

    function resetOtherClassPosition(activeIndex) {
        var $outerSlide = $("#outer > .swiper-wrapper > .swiper-slide");
        for (var i = 0; i < $outerSlide.length; i++) {
            if (i !== activeIndex) {
                if (inners[i]) {
                    inners[i].swipeTo(0);
                }
            }
        }
    }
}